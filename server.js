const express=require('express');

const { blogs,portfolio,link,productsLink }=require('./methods/handlers');

require('dotenv').config();

const app = express();


app.get('/blogs',blogs);
app.get('/portfolio',portfolio);
app.get('/link/:url',link);
app.get('/product-link/:url',productsLink);

app.listen(process.env.PORT, () => console.log('Server started at '+process.env.PORT));
