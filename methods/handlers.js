const axios=require('axios');
const textversion=require('textversionjs');

require('dotenv').config();

module.exports={
    blogs:async (req,res)=>{
        let {link}=req.query;
        const api=`${process.env.BACKEND}/singleBlog?url=${link}`;
        
        let meta=[];
        try{
          let {title,url,description,coverImg}=(await axios.get(api)).data.selected;
          coverImg=`${process.env.BACKEND}/imageStream/${coverImg}-share`;
          link=url=`${process.env.FRONTEND}/blogs/#/${url}`;
          
          meta=[
            `<meta charset="UTF-8">`,
            `<meta name="image" property="og:image" content="${coverImg}"/>`,
            '<meta property="fb:app_id" content="433456807257539"/>',
            `<title>${title}</title>`,
            `<meta http-equiv='cache-control' content='no-cache'/> 
            <meta http-equiv='expires' content='0'/> 
            <meta http-equiv='pragma' content='no-cache'/>`,
            `<meta property="og:type" content="article"`,
            `<meta property="og:title" content="${title.trim()}"/>`,
            `<meta property="og:description" content="${description.trim()}"/>`,
            `<meta property="og:image" content="${coverImg}"/>`,
            `<meta property="og:image:secure_url" content="${coverImg}"/>`,
            `<meta property="og:image:width" content="1400" />`,
            `<meta property="og:image:height" content="800" />`,
            `<meta property="og:image:secure" content="${coverImg}"/>`,
            `<meta name="twitter:title" content="${title.trim()}"/>`,
            `<meta name="twitter:description" content="${description.trim()}"/>`,
            `<meta name="twitter:image" content="${coverImg}"/>`,
            `<meta name="twitter:card" content="summary"/>`,
            
          ].join("");
      
      
        }catch(err){
          console.log(err);
          return res.send("Error");
        }
      
        let html=`<!DOCTYPE html>
        <html lang="en">
                <head>${meta}</head>
            <script>(function(){window.location="${link}"})()</script>
          </html>`
      
        return res.send(html);
      },
      
      portfolio:async (req,res)=>{
        let {link}=req.query;
        const api=`${process.env.BACKEND}/galleryFetchForward?url=${link}`;
        
        let meta=[];
        try{
          let {name,url,description,picture,typeData}=(await axios.get(api)).data.parent;
          picture=`${process.env.BACKEND}/imageStream/${picture}-share`;
          link=url=`${process.env.FRONTEND}/portfolio/#/${url}`;


          if(typeData=="file"){
            description=textversion(description);
          }

          meta=[
            `<meta charset="UTF-8">`,
            `<meta charset="UTF-8">`,
            `<meta name="viewport" content="width=device-width, initial-scale=1.0">`,
            `<title>${name}</title>`,
            `<meta property="og:type" content="article"/>`,
            `<meta property="og:title" content="${name.trim()}"/>`,
            `<meta property="og:description" content="${description.trim()}"/>`,
            `<meta property="og:image" content="${picture}"/>`,
            `<meta property="og:image:secure_url" content="${picture}"/>`,
            `<meta property="og:image:secure" content="${picture}"/>`,
            `<meta property="og:image:width" content="1400" />`,
            `<meta property="og:image:height" content="800" />`,
            `<meta name="twitter:title" content="${name.trim()}"/>`,
            `<meta name="twitter:description" content="${description.trim()}"/>`,
            `<meta name="twitter:image" content="${picture}"/>`,
            `<meta name="twitter:card" content="summary"/>`,
            '<meta property="fb:app_id" content="433456807257539"/>',
          ].join("");
    
      
        }catch(err){
          console.log(err);
          return res.send("Error");
        }

        
        let html=`<!DOCTYPE html>
        <html lang="en">
            <head>${meta}</head>
            <script>(function(){window.location="${link}"})()</script>
          </html>`
      
        return res.send(html);
      },


      link:async (req,res)=>{
        let {url}=req.params;
        const api=`${process.env.BACKEND}/link-generator-share?url=${url}`;
        
        let meta=[];
        try{
          let {title,url,description,picture,redirectUrl}=(await axios.get(api)).data;

          
          meta=[
            `<meta charset="UTF-8">`,
            `<meta charset="UTF-8">`,
            `<meta name="viewport" content="width=device-width, initial-scale=1.0">`,
            `<title>${title}</title>`,
            `<meta property="og:type" content="article"/>`,
            `<meta property="og:title" content="${title.trim()}"/>`,
            `<meta property="og:description" content="${description.trim()}"/>`,
            `<meta property="og:image" content="${picture}"/>`,
            `<meta property="og:image:secure_url" content="${picture}"/>`,
            `<meta property="og:image:secure" content="${picture}"/>`,
            `<meta property="og:image:width" content="2000" />`,
            `<meta property="og:image:height" content="1500" />`,
            `<meta name="twitter:title" content="${title.trim()}"/>`,
            `<meta name="twitter:description" content="${description.trim()}"/>`,
            `<meta name="twitter:image" content="${picture}"/>`,
            `<meta name="twitter:card" content="summary"/>`,
            '<meta property="fb:app_id" content="433456807257539"/>',
          ].join("");
    
          let html=`<!DOCTYPE html>
          <html lang="en">
              <head>${meta}</head>
              <script>(async function(){
                try{
                  await fetch('${process.env.BACKEND}/link-generator-share?url=${url}&click=1');
                }catch(e){}
                window.location="${redirectUrl}";
              })()</script>
            </html>`
        
          return res.send(html);
      
        }catch(err){
          console.log(err);
          return res.send("Error");
        }
      },

      productsLink:async (req,res)=>{
        let {url}=req.params;
        const api=`${process.env.BACKEND_PRODUCTS}/share?url=${url}`;
        
        let meta=[];
        try{
          let {title,url,description,picture,redirectUrl}=(await axios.get(api)).data;

          
          meta=[
            `<meta charset="UTF-8">`,
            `<meta charset="UTF-8">`,
            `<meta name="viewport" content="width=device-width, initial-scale=1.0">`,
            `<title>${title}</title>`,
            `<meta property="og:type" content="article"/>`,
            `<meta property="og:title" content="${title.trim()}"/>`,
            `<meta property="og:description" content="${description.trim()}"/>`,
            `<meta property="og:image" content="${picture.url}"/>`,
            `<meta property="og:image:secure_url" content="${picture.url}"/>`,
            `<meta property="og:image:secure" content="${picture.url}"/>`,
            `<meta property="og:image:width" content="2000" />`,
            `<meta property="og:image:height" content="1500" />`,
            `<meta name="twitter:title" content="${title.trim()}"/>`,
            `<meta name="twitter:description" content="${description.trim()}"/>`,
            `<meta name="twitter:image" content="${picture.url}"/>`,
            `<meta name="twitter:card" content="summary"/>`,
            '<meta property="fb:app_id" content="433456807257539"/>',
          ].join("");
    
          let html=`<!DOCTYPE html>
          <html lang="en">
              <head>${meta}</head>
              <script>(async function(){
                try{
                  await fetch('${process.env.BACKEND_PRODUCTS}/share?url=${url}&click=1');
                }catch(e){}
                window.location="${redirectUrl}";
              })()</script>
            </html>`
        
          return res.send(html);
      
        }catch(err){
          console.log(err);
          return res.send("Error");
        }
      }


}